listComponent.service("$dataService", function ($rootScope) {

    var self = this;

    self.getData = function (key) {
        return localStorage.getItem(key) ? JSON.parse(localStorage.getItem(key),
            function (key, value) {
                if (json.isDateString(value)) {
                    return convert.fromDateJsonStringToDate(value);
                }
                return value;
            }) : [];
    };

    self.addItem = function (key, item, idField) {
        var items = self.getData(key);
        if (items[0][idField]) {
            item[idField] = items[items.length - 1][idField] + 1;
        }
        items.push(item);
        localStorage.setItem(DATA_KEY, JSON.stringify(items));
        $rootScope.$broadcast("itemAdded");
    }

});