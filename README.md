# List component

### Important info
I've changed one of component design. 
Component by default shows all fields of single object from list and automatically recognise type of data (at the moment: text, numer, date, boolean). 
It possible to change which columns should be displayed by add/edit **display-columns** attribute for **list-view** directive.

### Necessary to run app
* **Chrome** or **Firefox** web browser (because of support `<input type="date">`)
* IDE with option/extension type **Open in Browser** / **Open in Live Server**
* Internet connection

### How to run app 
Because of using AngularJS from CDN the better way is:
1. clone project from gitlab repository
2. unpack archive with project
2. open project folder by some IDE eg. Visual Studio Code, WebStorm etc
3. from IDE, run **index.html** file by option/extension type **Open in Browser** / **Open in Live Server**