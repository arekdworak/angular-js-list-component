listComponent.controller("FormCtrl", function ($dataService) {

    var self = this;

    self.username;

    self.lastLogin;

    self.isAdmin;


    self.addItem = function () {
        var item = {
            username: self.username,
            lastLogin: self.lastLogin,
            isAdmin: !!self.isAdmin
        };

        $dataService.addItem(DATA_KEY, item, "id");

        self.username = null;
        self.lastLogin = null;
        self.isAdmin = false;
    }

});