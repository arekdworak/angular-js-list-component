listComponent.directive("paggingRow", function () {
    return {
        restrict: 'E',
        templateUrl: 'templates/paggingRow.tpl.html',
        replace: true,
        scope: {
            availablePageSizes: "=",
            pageSize: "=",
            getPagesInfo: "&",
            onPrevPageClick: "&",
            onNextPageClick: "&"
        }

    }
});