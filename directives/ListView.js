listComponent.directive("listView", function ($filter, $dataService, $rootScope) {
    return {
        restrict: 'E',
        templateUrl: 'templates/listView.tpl.html',
        replace: true,
        scope: {
            valuesKey: "@",
            mappingsKey: "@",
            availablePageSizes: "=",
            displayColumns: "=",
        },
        link: function ($scope, $elem, $attrs) {
            var allData = $dataService.getData($scope.valuesKey);
            var currentPage = 0;
            var pages = [];


            $scope.keys = $scope.displayColumns || Object.keys(allData[0]);
            $scope.mappings = JSON.parse(localStorage.getItem($scope.mappingsKey));
            $scope.sort = {
                key: $scope.keys[0],
                reverse: false
            };
            $scope.pageSize = $scope.availablePageSizes[0];

            $scope.setSort = function (key, reverse) {
                $scope.sort = {
                    key: key,
                    reverse: reverse
                }
            }

            $scope.prevPage = function () {
                if (currentPage > 0) {
                    currentPage--;
                    $scope.dataToShow = pages[currentPage].data;
                }
            }

            $scope.nextPage = function () {
                if (currentPage < pages.length - 1) {
                    currentPage++;
                    $scope.dataToShow = pages[currentPage].data;
                }
            }

            $scope.getPagesInfo = function () {
                return {
                    currentPage: currentPage,
                    start: pages[currentPage].start + 1,
                    end: pages[currentPage].end,
                    dataLength: allData.length,
                    lastPageNo: pages.length - 1
                }
            }

            var applySortAndPaging = function () {
                pages = [];
                currentPage = 0;
                var pagesCount = Math.floor(allData.length / $scope.pageSize);
                var sortedData = $filter("orderBy")(allData, $scope.sort.key, $scope.sort.reverse);
                if (allData.length % $scope.pageSize > 0) {
                    pagesCount++;
                }
                for (var i = 0; i < pagesCount; i++) {
                    var start = i * $scope.pageSize;
                    var end = i * $scope.pageSize + $scope.pageSize;
                    var page = {
                        data: sortedData.slice(start, end),
                        start: start
                    }
                    page.end = page.start + page.data.length
                    pages.push(page);
                }

                $scope.dataToShow = pages[currentPage].data;
            }

            var watcher = $scope.$watchGroup(["sort", "pageSize"], function () {
                applySortAndPaging();
            });

            var listener = $rootScope.$on("itemAdded", function () {
                allData = $dataService.getData($scope.valuesKey);
                applySortAndPaging();
            });

            $scope.$on("$destroy", function () {
                watcher();
                listener();
            })
        }

    }
});