listComponent.directive("cell", function () {
    return {
        restrict: 'E',
        templateUrl: 'templates/cell.tpl.html',
        scope: {
            value: "="
        },
        link: function ($scope, $elem, $attrs) {
            var type = typeof $scope.value;
            if (type == "object") {
                if (angular.isDate($scope.value)) {
                    type = "date";
                }
            }
            $scope.type = type;
        }

    }
});