listComponent.directive("headerCell", function () {
    return {
        restrict: 'E',
        templateUrl: 'templates/headerCell.tpl.html',
        replace: true,
        scope: {
            key: "=",
            label: "=",
            onSortChange: "&",
            sort: "="
        },
        link: function ($scope, $elem, $attrs) {
            $scope.reverse = $scope.sort.key == $scope.key ? $scope.sort.reverse : true;

            $scope.changeSort = function () {
                $scope.reverse = !$scope.reverse;
                $scope.onSortChange({
                    $key: $scope.key,
                    $reverse: $scope.reverse
                });
            }
        }

    }
});