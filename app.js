var listComponent = angular.module("ListComponent", []);


/** UTILS **/

var convert = {};
/**
 * Converts JSON date string to Date object
 *
 * @param {string} value
 * @returns {Date}
 */
convert.fromDateJsonStringToDate = function (value) {
    var time = Date.parse(value);
    if (!isNaN(time)) {
        return new Date(time);
    }
    return value;
};

var json = {};

/**
 * Returns true if value is date string, otherwise false
 *
 * @param {string} value
 * @returns {boolean}
 */
json.isDateString = function (value) {
    // JSON.stringify change all dates to string in format yyyy-MM-ddTHH:mm:sssZ
    return angular.isString(value) && value.match(/\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}.\d{3}Z/);
};


/** DATA INITIALIZATION */

var KEYS_MAPPING_KEY = "keysMapping";

if (!localStorage.getItem(KEYS_MAPPING_KEY)) {
    localStorage.setItem(KEYS_MAPPING_KEY, JSON.stringify({
        "id": "Id",
        "username": "User name",
        "lastLogin": "Last log in",
        "isAdmin": "Is admin"
    }));
}

var DATA_KEY = "listComponentData";

if (!localStorage.getItem(DATA_KEY)) {
    localStorage.setItem(DATA_KEY, JSON.stringify([{
            "id": 1,
            "username": "elawdham0",
            "lastLogin": "2018-06-21",
            "isAdmin": true
        },
        {
            "id": 2,
            "username": "cschnitter1",
            "lastLogin": new Date("2017-10-23"),
            "isAdmin": true
        },
        {
            "id": 3,
            "username": "sfowlie2",
            "lastLogin": new Date("2018-03-29"),
            "isAdmin": true
        },
        {
            "id": 4,
            "username": "eallridge3",
            "lastLogin": new Date("2018-05-12"),
            "isAdmin": true
        },
        {
            "id": 5,
            "username": "kfoxley4",
            "lastLogin": new Date("2017-08-16"),
            "isAdmin": false
        },
        {
            "id": 6,
            "username": "ssuero5",
            "lastLogin": new Date("2017-09-11"),
            "isAdmin": false
        },
        {
            "id": 7,
            "username": "zbuttle6",
            "lastLogin": new Date("2018-08-09"),
            "isAdmin": true
        },
        {
            "id": 8,
            "username": "mjeffery7",
            "lastLogin": new Date("2018-01-12"),
            "isAdmin": false
        },
        {
            "id": 9,
            "username": "bquestier8",
            "lastLogin": new Date("2017-06-18"),
            "isAdmin": true
        },
        {
            "id": 10,
            "username": "rbarkly9",
            "lastLogin": new Date("2018-08-25"),
            "isAdmin": false
        },
        {
            "id": 11,
            "username": "ameynella",
            "lastLogin": new Date("2018-10-07"),
            "isAdmin": true
        },
        {
            "id": 12,
            "username": "aretchlessb",
            "lastLogin": new Date("2017-12-09"),
            "isAdmin": true
        },
        {
            "id": 13,
            "username": "fclaceyc",
            "lastLogin": new Date("2018-07-12"),
            "isAdmin": false
        },
        {
            "id": 14,
            "username": "wneamed",
            "lastLogin": new Date("2017-12-25"),
            "isAdmin": false
        },
        {
            "id": 15,
            "username": "shaleye",
            "lastLogin": new Date("2017-08-13"),
            "isAdmin": false
        },
        {
            "id": 16,
            "username": "jcubleyf",
            "lastLogin": new Date("2018-01-11"),
            "isAdmin": false
        },
        {
            "id": 17,
            "username": "emarrittg",
            "lastLogin": new Date("2017-06-06"),
            "isAdmin": false
        },
        {
            "id": 18,
            "username": "gfackrellh",
            "lastLogin": new Date("2018-06-30"),
            "isAdmin": true
        },
        {
            "id": 19,
            "username": "obointoni",
            "lastLogin": new Date("2018-08-25"),
            "isAdmin": true
        },
        {
            "id": 20,
            "username": "kchampnissj",
            "lastLogin": new Date("2017-06-02"),
            "isAdmin": true
        }
    ]));

}